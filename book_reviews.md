Boneshaker by Christie Priest [aug2012-oct2012]
In an approximate version of the Seattle gold rush, a true steampunk story 
involving a deadly gas, drugs, leather and a cloudy and misty atmosphere. 
Did I mention zombies? Or airships powered by steam? 
Overall a nice and short read with a good story. Rating 4.5/5

Don't Make Me Think by Steve Krug [oct2012]
The "bible" of web usability in its second incarnation. A couple of ideas or reminders from the book:
+ Make the site natural to the user and his actions
+ Build billboards not newspapers
+ Make stuff visible and easy to determine what's its function to the user
+ "Get rid of half the words on each page, then get rid of what's left"
+ Home page is really an important starting point in the design, although everybody wants to have its saying about it. 
+ make clear what's the mission of the site, easily done through displaying a tagline (5-8 words)
+ make sure the home page conveys just the main objective of the site and not an in-depth review of what the site does
+ have a clear separation of the main tagline, categories, description, search and others
+ usability: testing earlier is better and cheaper than doing it at the very end of the project
+ to get a new perspective on the site you're building, you must test it
+ testing some parts by even 1 user is better than no testing at all
+ focus groups help establishing the target user group of the site before the design phase starts
+ try to involve different people from different teams in order to get more traction and involvement during the (usability) testing
+ ask the tester to do some specific tasks besides 
+ After the walkthroughs review/go over the notes as soon as possible

The Lean Startup by [jan2013-]
+ Going after a couple of ideas of what make a startup tick, and how can you avoid falling in the most common pitfals of the startup movement.
+ Talking about `validated learning' where all the steps you take are directed by input coming from the customers and not from the bussiness owners. A couple of examples presented like the IMVU service, the laundry in India, or Kodak photo share which all started with a basic product, and improving that by getting input from customers instead of having a plan before going to market.
